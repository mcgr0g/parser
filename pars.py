# -*- coding: utf8 -*-

import sys
from codecs import getwriter
from json import dumps
from datetime import datetime
from signal import signal, SIGINT
import requests
import re
import urlparse
from lxml import etree, html

# ---CONSTANTS & DATA--------------
MODE_DEBUG = False
TARGET_HOST = 'http://www.kszn.ru'
# TARGET_HOST = 'https://p.ya.ru/omsk'
RUBRIC_ID = 184106322  # Социальная помощь
NEED_TAB = False  # Еще один костыль, что бы дорисовывать xml с табами
HEADERS = {'User-Agent': "Opera/9.80 (Macintosh; Intel Mac OS X 10.8.0) Presto/2.12.363 Version/12.50"}
WAIT = 30  # timeout in sec
ID_COUNTER = 1


# ###CONSTANTS & DATA##############


def warn(*args):
    """
    функа для записи ошибок
    """
    sys.stderr.write(' '.join(map(str, args)) + '\n')


def wout(*args):
    for arg in args:
        if NEED_TAB:
            if isinstance(arg, basestring):
                count_caret = arg.count('\n') - 1
                # именно столько - закрывающий тэг должен быть c табом
                temp = arg.replace('\n', '\n  ', count_caret)
                temp = '  ' + temp
            else:
                temp = arg
            sys.stdout.write(temp)
        else:
            sys.stdout.write(arg + "\n")


def skeleton_start():
    global NEED_TAB
    wout('<?xml version="1.0" encoding="UTF-8"?>')
    wout('<companies xmlns:xi="http://www.w3.org/2001/XInclude" version="2.1">')
    NEED_TAB = True


def skeleton_end():
    global NEED_TAB
    NEED_TAB = False
    print ("</companies>")


def get_district():
    """
    возвращает список округов  виде ссылок
    """
    root_page = requests.get(TARGET_HOST, timeout=WAIT)
    root_content = root_page.content
    root_encoding = root_page.encoding
    content_converted = root_content.decode(root_encoding, 'replace')
    root_page_tree = html.fromstring(content_converted)
    # Организации по округам
    # distict_link = root_page_tree.xpath(".//*[@id='left']/div/a[2]/@href")
    # взяли первую ссылку из _списка_
    district_list_link = root_page_tree.xpath(u".//*[@id='footer']/div/a[contains(text(), 'округ')]/@href")[0]
    district_list_page = requests.get(district_list_link, timeout=WAIT)
    district_list_content = district_list_page.content.decode(district_list_page.encoding, 'replace')
    district_list_tree = html.fromstring(district_list_content)
    # вообще сразу можно было пихнуть страницу http://www.kszn.ru/resursy_po_okrugam и не выкобениваться
    distict_list_raw = district_list_tree.xpath(".//*[@id='content_9']/div[3]/h3[1]/a/@href")
    distict_list = list()
    for district in distict_list_raw:
        distict_list.append(str(district))
    return distict_list


def get_company_per_district(district_url):
    """
    возврващает список ссылкок на страницы учереждений в округе
    :param district_url: ссылка на страницу округа
    """
    if not isinstance(district_url, basestring): return  # только строки
    district_page = requests.get(district_url, timeout=WAIT)
    district_content = district_page.content.decode(district_page.encoding, 'replace')
    district_tree = html.fromstring(district_content)
    company_link_tree = district_tree.xpath(u".//*[@id='content_9']/div[3]/a[contains(text(), 'Подробнее')]/@href")
    company_link_list = list()
    for link in company_link_tree:
        company_link_list.append(str(link))
    return company_link_list


def get_district_company_params(company_url):
    """
    принимает ссылку на страницу компании из округа на целевом хосте
    возврашаяет элемент etree со параметрами для обработки/печати
    ну или печатеает :)
    """
    global ID_COUNTER
    if not isinstance(company_url, basestring): return  # только строки
    district_page = requests.get(company_url, timeout=WAIT)
    company_content = district_page.content.decode(district_page.encoding, 'replace')
    company_raw_tree = html.fromstring(company_content)

    # записываем в потороха что может https://gist.github.com/printsesso/f6c9bf95c32e464eeefb#Пример-xml-файла
    company_res_tree = etree.Element("company")

    company_id_tree = etree.SubElement(company_res_tree, 'company-id')  # <company-id>770704034</company-id>
    company_id_tree.text = str(ID_COUNTER)
    ID_COUNTER += 1

    # название
    company_name_tree = etree.SubElement(company_res_tree, 'name')  # <name lang="ru">Отель Якорь ООО</name>
    company_name_tree.set('lang', 'ru')
    name_raw = company_raw_tree.xpath(u".//*[@id='content_9']/div[3]/h1/text() | .//*[@id='content_9']/div[3]/h1/*/text()")
    if not len(name_raw):
        if MODE_DEBUG: warn('xpath not found element of company_name at %s' % company_url)
        return
    company_name_tree.text = name_raw[0]  # в сраном юникоде

    # адрес <address lang="ru">город Екатеринбург, проспект Ленина, 101а</address>
    address_raw = company_raw_tree.xpath(u".//*[@id='content_9']/div[3]/p[contains(.,'Адрес')]/text()")
    if not len(address_raw):
        if MODE_DEBUG: warn('xpath not found element of address_raw at %s' % company_url)
        return
    address_full = address_raw[0]
    # индекс 123456
    post_index_tree = etree.SubElement(company_res_tree, 'post-index')
    address_tree = etree.SubElement(company_res_tree, 'address')
    post_index_tree.text, address_tree.text = parse_address(address_full)
    if post_index_tree.text == 0:
        if MODE_DEBUG: warn('xpath not found element of post_index_tree at %s' % company_url)
        return
    if address_tree.text == '':
        if MODE_DEBUG: warn('xpath not found element of address_tree at %s' % company_url)
        return

    # рубрика <rubric-id>184106414</rubric-id>
    rubric_tree = etree.SubElement(company_res_tree, 'rubric-id')
    rubric_tree.text = str(RUBRIC_ID)

    # сайт <url>http://www.yakor-anapa.ru</url>
    url_tree = etree.SubElement(company_res_tree, 'url')
    url_raw_tree = company_raw_tree.xpath(u".//*[contains(text(), 'источник')]/../a/@href")
    if not len(url_raw_tree):
        if MODE_DEBUG: warn('xpath not found element url_raw_tree at %s' % company_url)
        return
    resolve_deep = 4  # количество рекурсий
    url_tree.text = resolve_url(url_raw_tree[0], resolve_deep)

    if url_tree.text == '':
        if MODE_DEBUG: warn('resolve_url() cannot resolve url_raw_tree at %s' % company_url)
        return

    # дата актуалиации <actualization-date>13.05.2013</actualization-date>
    act_date_tree = etree.SubElement(company_res_tree, 'actualization-date')
    act_date_tree.text = datetime.now().strftime('%d.%m.%Y')

    wout(etree.tounicode(company_res_tree, pretty_print=True))
    return  # company_res_tree


def parse_address(address_string):
    # address_string = "105064, г. Москва, Гороховский пер., д. 5, стр. 11 "
    # индекс всегда ссотовит из 6 цифр
    index = re.findall('\d{6}', address_string)[0]
    if not index: index = 0 # не нашли
    # выпиливаем ", "
    temp = re.split(index + ',* *', address_string)
    address = ''.join(temp)
    return index, address


def resolve_url(company_url, resolve_deep):
    resolve_deep_inner = resolve_deep - 1
    if resolve_deep_inner <= 0:
        warn('too much loops at resolve_url()')
        return ""
    entry = re.search(TARGET_HOST, company_url)
    if not entry:  # а есть ли целевой домен в ссылке - нетю
        parsed_uri_company_url = urlparse.urlparse(company_url)
        # внезапно мы можем оказаться на другом хосте, тогда надо посмореть где это мы
        rum_host = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri_company_url)
        if not len(parsed_uri_company_url.netloc):
            # мы все еще на своем хосте, просто цмска дает только конец uri, без хоста
            full_url = TARGET_HOST + company_url
        else:  #ури то подозрительного хоста вычислили, сравним теперь с нашим целевым
            parsed_uri_target_host = urlparse.urlparse(TARGET_HOST)
            if parsed_uri_company_url.netloc == parsed_uri_target_host.netloc:  # мы дома и у нас просто длинный ковявый урл
                full_url = company_url
            else:  # ну афигеть, мы всетаки вышли наружу
                #resolve_deep_inner = 0 # ухим и не возвращаемся в эту рекурси
                return rum_host
    else:  # целевой домен есть в переданноый сслыке
        full_url = company_url
    target_page = requests.get(full_url, timeout=WAIT)
    # проверка, что ссылка то фурычит
    if target_page.status_code == 200:
        entry = re.search(TARGET_HOST, full_url)
        if entry:
            # срань господня с петлями на свой же хост
            target_content = target_page.content
            target_encoding = target_page.encoding
            content_converted = target_content.decode(target_encoding, 'replace')
            target_content_tree = html.fromstring(content_converted)
            new_target = target_content_tree.xpath(".//*[@id='content_9']/div[3]/*/a/@href")
            if not len(new_target):
                if MODE_DEBUG: warn('resolve_url() cannot resolve url loop at %s' % full_url)
                return ""
            new_target_link = resolve_url(new_target[0], resolve_deep_inner)
            if MODE_DEBUG: warn('resolved!!!!! ', new_target_link)
            return new_target_link
        else:  # ссылка кудато на сторонний ресурс
            return full_url
    else:
        # срань господня c битыми ссылками
        if MODE_DEBUG: warn('there is broken link %s' % company_url)
        return ""
    return ""


def main():
    # MODE_DEBUG = True

    if MODE_DEBUG:
        # sys.stdout = open('data.xml', 'w', )
        # sys.stderr = open('err.log', 'w')
            warn(str(datetime.now()) + ' LOG START')
    else:
        sys.stderr = getwriter('utf-8')(sys.stderr, 'strict')
        sys.stdout = getwriter('utf-8')(sys.stdout, 'strict')

    # костыль, что бы не держать в памяти все дерево
    skeleton_start()

    try:
        districts = get_district()[1]
        company_links = get_company_per_district(districts)
        company_xml = get_district_company_params(company_links[1])
        # wout(etree.tounicode(company_xml, pretty_print=True))

        skeleton_end()

    finally:
        pass
    # except Exception as e:
    #     exc_traceback = sys.exc_info()[2]
    #     filename = line = None
    #     while exc_traceback is not None:
    #         f = exc_traceback.tb_frame
    #         line = exc_traceback.tb_lineno
    #         filename = f.f_code.co_filename
    #         exc_traceback = exc_traceback.tb_next
    #     warn(dumps({
    #         'error': True,
    #         'details': {
    #             'message': str(e),
    #             'file': filename,
    #             'line': line
    #         }
    #     }, ensure_ascii=False) + "\n")
    #     exit(1)

    if MODE_DEBUG:
        warn(str(datetime.now()) + ' LOG END')
        sys.stdout.close()
        sys.stderr.close()


if __name__ == "__main__":
    def signal_handler(signal, frame):
        sys.exit(0)


    signal(SIGINT, signal_handler)
    main()
